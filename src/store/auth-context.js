import React, { useState, useEffect } from "react";
import axios from "axios";

const AuthContext = React.createContext({
  isLogin: false,
  login: () => {},
  logout: () => {},
});

const showMe = async () => {
  try {
    let response = await axios.get("/api/v1/users/showMe");
    console.log("first show me", response);
    if (response.statusText === "OK") {
      return response.data;
    }
  } catch (error) {
    console.log("error in login");
  }
  return {};
};

export const AuthContextProvider = (props) => {
  const [showMeResponse, setshowMeResponse] = useState(null);
  const [userLoggedIn, setuserLoggedIn] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      const tmpResponse = await showMe();
      console.log("shashi's useEffect data from show me", tmpResponse);
      if (Object.keys(tmpResponse).length > 0) {
        setshowMeResponse(tmpResponse);
        setuserLoggedIn(true);
      }
    };
    fetchData();
    if (showMeResponse && Object.keys(showMeResponse).length > 0) {
      setuserLoggedIn(true);
    }
  }, []);

  console.log("shashi's data may be a right one not promise", showMeResponse);



  const logoutHandler = () => {
    try {
      axios.get("/api/v1/auth/logout").then((response) => {
        if (response.statusText === "OK") {
          setuserLoggedIn(false);
        }
      });
    } catch (error) {
      console.log("error in logging out");
    }
  };

  const loginHandler = () => {
    try {
      axios.get("/api/v1/users/showMe").then((response) => {
        console.log("first", response);
        if (response.statusText === "OK") {
          if (Object.keys(response.data).length > 0) {
            setuserLoggedIn(true);
          }
        }
      });
    } catch (error) {
      console.log("error in login");
    }
  };

  const contextValue = {
    isLogin: userLoggedIn,
    login: loginHandler,
    logout: logoutHandler,
  };
  return (
    <AuthContext.Provider value={contextValue}>
      {props.children}
    </AuthContext.Provider>
  );
};
export default AuthContext;
