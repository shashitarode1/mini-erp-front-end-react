import { useContext, useRef } from 'react';
import { useNavigate } from 'react-router-dom';
import AuthContext from '../../store/auth-context';
import classes from './ProfileForm.module.css';

const ProfileForm = () => {
  const passwordRef = useRef();

  const authCtx = useContext(AuthContext);
  const navigate = useNavigate();

  const submitHandler = (event)=>{
    event.preventDefault();
    const password = passwordRef.current.value;

    fetch('https://identitytoolkit.googleapis.com/v1/accounts:update?key=AIzaSyCJZGkb-QsHOh8x70k8JDdiL42hY444eJ8',{
      method:"POST",
      body:JSON.stringify({
        idToken:authCtx.token,
        password:password,
        returnSecureToken:true
      })
    })
    .then(async (response)=>{
      if (response.ok){
        return response.json()
      }
      else{
        return response.json().then(data=>{
          let emessage = ""
          if (data && data.error && data.error.message){
            // alert(data.error.message)
            emessage += data.error.message
          }
          throw new Error(emessage);
        })
      }
    })
    .then((data)=>{
      alert("Successfuly Chnaged the password")
      navigate('/')
    })
    .catch(err=>{
      alert(err.message)
    })


  }

  
  return (
    <form className={classes.form} onSubmit={submitHandler}>
      <div className={classes.control}>
        <label htmlFor='new-password'>New Password</label>
        <input type='password' id='new-password' ref={passwordRef}/>
      </div>
      <div className={classes.action}>
        <button>Change Password</button>
      </div>
    </form>
  );
}

export default ProfileForm;
