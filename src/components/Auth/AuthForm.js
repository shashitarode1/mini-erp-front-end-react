import { useContext, useRef, useState } from "react";
import { useNavigate } from "react-router-dom";
import AuthContext from "../../store/auth-context";
import axios from 'axios';

import classes from "./AuthForm.module.css";
// import { createGlobalStyle } from 'styled-components';

const AuthForm = () => {
  const [isLogin, setIsLogin] = useState(true);
  // const [isLoading,setisLoading] = useState(false);
  const authCtx = useContext(AuthContext);
  

  const switchAuthModeHandler = () => {
    console.log("shashi tarode");
    setIsLogin((prevState) => !prevState);
    console.log("first");
  };

  const emailRef = useRef();
  const nameRef = useRef();
  const passwordRef = useRef();
  const navigate = useNavigate();

  const submitHandler = (even) => {
    // axios.defaults.withCredentials = true;
    console.log("shashi tarode");
    even.preventDefault();
    const email = emailRef.current.value;
    let name = "";
    const password = passwordRef.current.value;
    console.log(email, password);
    let url = "";
    if (isLogin) {
      url =
        "/api/v1/auth/login";
    } else {
      name = nameRef.current.value
      url = "/api/v1/auth/register";
    }
    axios.post(url, {
      name,
      email,
      password,
    })
      .then(async (response) => {
        console.log("response from the backedn", response);
        if (response.statusText === "OK") {
          return response.data;
        } else {
          return response.json().then((data) => {
            let emessage = "";
            if (data && data.error && data.error.message) {
              // alert(data.error.message)
              emessage += data.error.message;
            }
            throw new Error(emessage);
          });
        }
      })
      .then((data) => {
        console.log(data, "shashi next step");
        authCtx.login();
        navigate("/");
      })
      .catch((err) => {
        alert(err.message);
      });
  };

  return (
    <section className={classes.auth}>
      <h1>{isLogin ? "Login" : "Sign Up"}</h1>
      <form onSubmit={submitHandler}>
        {!isLogin && (
          <div className={classes.control}>
            <label htmlFor="name">Your Name</label>
            <input type="name" id="name" required ref={nameRef} />
          </div>
        )}
        <div className={classes.control}>
          <label htmlFor="email">Your Email</label>
          <input type="email" id="email" required ref={emailRef} />
        </div>
        <div className={classes.control}>
          <label htmlFor="password">Your Password</label>
          <input type="password" id="password" required ref={passwordRef} />
        </div>
        <div className={classes.actions}>
          <button>{isLogin ? "Login" : "Create Account"}</button>
          <button
            type="button"
            className={classes.toggle}
            onClick={switchAuthModeHandler}
          >
            {isLogin ? "Create new account" : "Login with existing account"}
          </button>
        </div>
      </form>
    </section>
  );
};

export default AuthForm;
