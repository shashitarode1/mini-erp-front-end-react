import { useContext } from "react";
import { Link } from "react-router-dom";
import AuthContext from "../../store/auth-context";
import * as React from "react";
import Box from "@mui/material/Box";
import SwipeableDrawer from "@mui/material/SwipeableDrawer";
// import Button from "@mui/material/Button";
import List from "@mui/material/List";
import Divider from "@mui/material/Divider";
import ListItem from "@mui/material/ListItem";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import InboxIcon from "@mui/icons-material/MoveToInbox";
import MailIcon from "@mui/icons-material/Mail";

import classes from "./MainNavigation.module.css";

const MainNavigation = () => {
  const authCtx = useContext(AuthContext);

  const logoutHandler = () => {
    authCtx.logout();
  };

  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event &&
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const listToBeShown = () => {
    if (!authCtx.isLogin) {
      return (
        <List>
          <Link to="/">
            <ListItem>
              <ListItemButton>
                <ListItemIcon>
                  {2 % 2 === 0 ? <InboxIcon /> : <MailIcon />}
                </ListItemIcon>
                <ListItemText primary="Home " />
              </ListItemButton>
            </ListItem>
          </Link>
          <Link to="/auth">
            <ListItem>
              <ListItemButton>
                <ListItemIcon>
                  {2 % 2 === 0 ? <InboxIcon /> : <MailIcon />}
                </ListItemIcon>
                <ListItemText primary="Login" />
              </ListItemButton>
            </ListItem>
          </Link>
        </List>
      );
    }
    return (
      // <List>
      //   {["All mail", "Trash", "Spam"].map((text, index) => (
      //     <ListItem key={text} disablePadding>
      //       <ListItemButton>
      //         <ListItemIcon>
      //           {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
      //         </ListItemIcon>
      //         <ListItemText primary={text} />
      //       </ListItemButton>
      //     </ListItem>
      //   ))}
      // </List>
      <List>
      <Link to="/">
        <ListItem>
          <ListItemButton>
            <ListItemIcon>
              {2 % 2 === 0 ? <InboxIcon /> : <MailIcon />}
            </ListItemIcon>
            <ListItemText primary="Home " />
          </ListItemButton>
        </ListItem>
      </Link>
      <Link to="/profile">
        <ListItem>
          <ListItemButton>
            <ListItemIcon>
              {2 % 2 === 0 ? <InboxIcon /> : <MailIcon />}
            </ListItemIcon>
            <ListItemText primary="profile" />
          </ListItemButton>
        </ListItem>
      </Link>
    </List>
    );
  };

  const list = (anchor) => (
    <Box
      sx={{ width: anchor === "top" || anchor === "bottom" ? "auto" : 250 }}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      {listToBeShown()}
      <Divider />
      {/* <List>
        {["All mail", "Trash", "Spam"].map((text, index) => (
          <ListItem key={text} disablePadding>
            <ListItemButton>
              <ListItemIcon>
                {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
              </ListItemIcon>
              <ListItemText primary={text} />
            </ListItemButton>
          </ListItem>
        ))}
      </List> */}
    </Box>
  );

  return (
    <header className={classes.header}>
      <IconButton
        color="inherit"
        aria-label="open drawer"
        onClick={toggleDrawer("left", true)}
        edge="start"
        sx={{
          marginRight: 5,
        }}
      >
        <MenuIcon />
      </IconButton>
      {/* <Button onClick={toggleDrawer("left", true)}>{"left"}</Button> */}
      <Link to="/">
        <div className={classes.logo}>Mini ERP </div>
      </Link>
      <nav>
        <ul>
          {!authCtx.isLogin && (
            <li>
              <Link to="/auth">Login</Link>
            </li>
          )}
          {authCtx.isLogin && (
            <li>
              <Link to="/profile">Profile</Link>
            </li>
          )}
          {authCtx.isLogin && (
            <li>
              <button onClick={logoutHandler}>Logout</button>
            </li>
          )}
        </ul>
      </nav>
      <SwipeableDrawer
        anchor={"left"}
        open={state["left"]}
        onClose={toggleDrawer("left", false)}
        onOpen={toggleDrawer("left", true)}
      >
        {list("left")}
      </SwipeableDrawer>
    </header>
  );
};

export default MainNavigation;
