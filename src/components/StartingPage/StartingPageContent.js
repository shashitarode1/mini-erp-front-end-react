import SwipeableTextMobileStepper from '../Layout/SwipeableTextMobileStepper';
import classes from './StartingPageContent.module.css';

const StartingPageContent = () => {
  return (
    <section className={classes.starting}>
      <SwipeableTextMobileStepper/>
      <h1>Welcome on Board!</h1>
    </section>
  );
};

export default StartingPageContent;
